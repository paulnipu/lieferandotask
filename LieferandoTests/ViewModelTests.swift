//
//  LieferandoTests.swift
//  LieferandoTests
//
//  Created by Arup Paul on 19/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import XCTest
import RealmSwift
import RxSwift

@testable import Lieferando

class ViewModelTests: XCTestCase {
    
    var disposeBag = DisposeBag.init()
    var realm:Realm?
    
    override func setUp() {
        
        realm = try! Realm()
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    
    func testParseData() {
        
        let testExpectation = expectation(description: "At least more then one restaurent will save")
        ViewModel.sharedInstance.parseData(errorCallback: nil)
        ViewModel.sharedInstance.restaurentList.asObservable().subscribe{ _ in
            testExpectation.fulfill()
            }.addDisposableTo(disposeBag)
        waitForExpectations(timeout: 5, handler: {[unowned self] error in
            XCTAssert(self.realm?.objects(Restaurent.self).count ?? 0 > 0, "No Club Loaded")
            self.disposeBag = DisposeBag.init()
        })
        
    }
    
    func testSort(){
        ViewModel.sharedInstance.sortData(by: .DELIVERY_COSTS, ascending: .ASC)
        let testExpectation = expectation(description: "Sushi One should be on top")
        ViewModel.sharedInstance.restaurentList.asObservable().subscribe{ _ in
            testExpectation.fulfill()
            
            }.addDisposableTo(disposeBag)
        
        waitForExpectations(timeout: 5, handler: { error in
            
            XCTAssert(ViewModel.sharedInstance.restaurentList.value.first?.name == "Sushi One", "Sort by best match failed")
            self.disposeBag = DisposeBag.init()
        })
        
    }
    
    func testSearch(){
        ViewModel.sharedInstance.searchRestaurent(searchParam: "sushi")
        let testExpectation = expectation(description: "Only 4 restaurent should be there")
        ViewModel.sharedInstance.restaurentList.asObservable().subscribe{ _ in
            testExpectation.fulfill()
            }.addDisposableTo(disposeBag)
        waitForExpectations(timeout: 5, handler: { error in
            
            XCTAssert(ViewModel.sharedInstance.restaurentList.value.count == 4, "Search Failed")
            self.disposeBag = DisposeBag.init()
        })
    }
    
    func testFavorite(){
        var restaurent = realm?.object(ofType: Restaurent.self, forPrimaryKey: "Sushi One")
        try! realm?.write {
            restaurent?.favourite = false
        }
        ViewModel.sharedInstance.toggleFavourite(restaurent: restaurent!)
        let testExpectation = expectation(description: "Favorite restaurent on top")
        ViewModel.sharedInstance.restaurentList.asObservable().subscribe{ _ in
            testExpectation.fulfill()
            }.addDisposableTo(disposeBag)
        waitForExpectations(timeout: 5, handler: { error in
            
            XCTAssert(ViewModel.sharedInstance.restaurentList.value.first?.name == "Sushi One", "Search Failed")
            self.disposeBag = DisposeBag.init()
        })
        restaurent = realm?.object(ofType: Restaurent.self, forPrimaryKey: "Sushi One")
        XCTAssert(restaurent?.favourite ?? false,"Sushi One is not favorite")
        
    }
    
    
    
}
