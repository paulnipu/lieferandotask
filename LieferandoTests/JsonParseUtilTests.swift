//
//  JsonParseUtilTest.swift
//  Lieferando
//
//  Created by Arup Paul on 23/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import XCTest
import RxSwift
import ObjectMapper

@testable import Lieferando

class JsonParserUtilTests: XCTestCase {
    
    var disposeBag = DisposeBag()
    
    override func setUp() {
        super.setUp()
    }
    
    func testJsonParseSucess(){
        JsonParserUtil.sharedInstance.parseJson("{\"restaurants\":[{\"name\":\"Test Restaurant\",\"status\":\"open\"}]}")
            .subscribe(onNext: { (responsemodel) in
                debugPrint(responsemodel.restaurents.first?.name ?? " ")
                XCTAssert(responsemodel.restaurents.first?.name ?? " "  == "Test Restaurant", "Json Parse failed")
            }, onError: nil, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
    }
    
    func testJsonParseFailed(){
        JsonParserUtil.sharedInstance.parseJson("{name:Test_Failed Restaurent status:'open'}")
            .subscribe(onNext: nil, onError: {error in
                debugPrint(error)
                XCTAssertNotNil(error)
            }, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
    }
}
