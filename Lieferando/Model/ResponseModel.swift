//
//  ResponseModel.swift
//  Lieferando
//
//  Created by Arup Paul on 19/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import Foundation
import ObjectMapper



class ResponseModel : Mappable{

    var restaurents:[Restaurent] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        restaurents <- map["restaurants"]
    }
}
