//
//  SortOptions.swift
//  Lieferando
//
//  Created by Arup Paul on 20/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import Foundation


enum SortOptions : String{
    case BEST_MATCH = "bestmatch"
    case NEWEST = "newest"
    case RATING_AVERAGE = "ratingaverage"
    case DISTANCE = "distance"
    case POPULARITY = "popularity"
    case AVERAGE_PRODUCT = "averageproductprice"
    case DELIVERY_COSTS = "deliverycosts"
    case MIN_COST = "mincost"
    
    static let allValues = [BEST_MATCH, NEWEST, RATING_AVERAGE, DISTANCE, POPULARITY, AVERAGE_PRODUCT, DELIVERY_COSTS, MIN_COST]
}

enum SortStyle : NSNumber{
    case DESC = 0
    case ASC
}

enum CustomError:Error{
    case FAILED_TO_PARSE
    
}

