//
//  Restaurent.swift
//  Lieferando
//
//  Created by Arup Paul on 19/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Restaurent : Object, Mappable{
    
    dynamic var name:String?
    
    dynamic var status : String?
    
    dynamic var sortstatus : Int = 0
    
    dynamic var favourite : Bool = false
    
    //Sorting Values
    
    dynamic var bestmatch : Float = 0.0
    
    dynamic var newest : Float = 0.0
    
    dynamic var ratingaverage : Float = 0.0
    
    dynamic var distance : Float = 0.0
    
    dynamic var popularity : Float = 0.0
    
    dynamic var averageproductprice : Float = 0.0
    
    dynamic var deliverycosts : Float = 0.0
    
    dynamic var mincost : Float = 0.0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        
        status <- map["status"]
        
        //Sorting Values
        
        bestmatch <- map["sortingValues.bestMatch"]
        
        newest <-  map["sortingValues.newest"]
        
        ratingaverage <- map["sortingValues.ratingAverage"]
        
        distance <- map["sortingValues.distance"]
        
        popularity <- map["sortingValues.popularity"]
        
        averageproductprice <- map["sortingValues.averageProductPrice"]
        
        deliverycosts <- map["sortingValues.deliveryCosts"]
        
        mincost <- map["sortingValues.minCost"]
        
        sortstatus = ((status == "open") ? 0 : (status == "order ahead") ? 1 : 2)
        

    }
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
    /*override class func ignoredProperties() -> [String] {
        return ["sortstatus"]
    }*/
}
