//
//  ViewController.swift
//  Lieferando
//
//  Created by Arup Paul on 19/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBarTopLayoutCostrain: NSLayoutConstraint!
    
    @IBOutlet weak var btnSearch: UIBarButtonItem!
    
    @IBOutlet weak var btnSort: UIBarButtonItem!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var btnSortStyle: UIBarButtonItem!
    
    
    
    let viewModel = ViewModel.sharedInstance
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupTableView()  //Populate Restaurent List in tableView
        
        setupSearch()      //Search functionalities
        
        setupSort()        // Sort functionlities
        
        setupSortStyle()   // Ascending or Descending sort
        
        viewModel.parseData {[unowned self] (error) in
            if let _ = error {
                let alert = UIAlertController(title: "Error", message: "Some thing went wrong, when parse the JSON", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        } //Start parsing Data
        
    }
    
    /**
        Description
        Bind tableview with Restaurent List to show the changes immidiately
     
     **/
    private func setupTableView(){
        viewModel.restaurentList.asObservable()
            .map{[unowned self] val -> [Restaurent] in
                if val.count <= 0{
                    self.tableView.showNOitem(message: "No Restaurnt to show")
                }else{
                    self.tableView.hideNoItemMessage()
                }
                return val
            }
            .bindTo(tableView.rx.items(cellIdentifier: "Cell", cellType: CustomCell.self)) {[unowned self] (row, restaurent, cell) in
                cell.setupData(restaurent: restaurent, sortCriteria: self.viewModel.sortCriteria)
                cell.favClicked {[unowned self] (selectedRestaurent) in
                    
                    if selectedRestaurent != nil{
                        self.viewModel.toggleFavourite(restaurant: selectedRestaurent!)
                    }
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    /**
        Description 
        Connect UIsearchbar Text property to View Model Serach Criterial Variable.
        Anychanges on UISearchBar text reflect on searchCriteria Variable and
        search function on viewmodel will call. 
     
        Also CancelButton action for show/hide search Bar
     
        - SearchCriteria is a binded with searchFunction
     
     **/
    private func setupSearch(){
        searchBar
            .rx
            .text
            .orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .bindTo(viewModel.searchCriteria).addDisposableTo(disposeBag)
        
        
        searchBar
            .rx
            .cancelButtonClicked
            .bindNext {
                self.toggleSearchBarVisibility()
            }.addDisposableTo(disposeBag)
        
    }
    
    /**
        Description
        Sort by Ascending or Descending order Actionsheet
        Show an actionsheet for choose the option
        
        Also it handles icon changing according to selection
    **/
    private func setupSortStyle(){
        btnSortStyle
            .rx
            .tap
            .bindNext{
                let actionSheetController = UIAlertController(title: "Sort Style", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
                let actionASC = UIAlertAction(title: "Ascending", style: .default, handler: {[unowned self] action  in
                    self.viewModel.sortData(by: self.viewModel.sortCriteria, style: .ASC)
                    self.btnSortStyle.image = UIImage(named: "asc")
                })
                let actionDESC = UIAlertAction(title: "Descending", style: .default, handler: {[unowned self] action  in
                    self.viewModel.sortData(by: self.viewModel.sortCriteria, style: .DESC)
                    self.btnSortStyle.image = UIImage(named: "desc")
                })
                actionSheetController.addAction(actionASC)
                actionSheetController.addAction(actionDESC)
                actionSheetController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(actionSheetController, animated: true, completion: nil)
                
            }.addDisposableTo(disposeBag)
    }
    
    /**
        Descriprtion
        Show a list of sortCriteria.
        Make a selection calls the required function to sort the data. Sorted data will trigger via the observable.
     
     **/
    private func setupSort(){
        btnSort
            .rx
            .tap
            .bindNext{[unowned self] in
                let actionSheetController = UIAlertController(title: "Sort", message: "Choose Sort methods", preferredStyle: UIAlertControllerStyle.actionSheet)
                
                SortOptions.allValues.forEach{
                    actionSheetController.addAction(UIAlertAction(title: $0.rawValue.capitalized, style: .default, handler: {[unowned self] (action) -> () in
                        self.viewModel.sortData(by:SortOptions(rawValue: (action.title?.lowercased())!)!, style: self.viewModel.sortType)
                    }))
                }
                actionSheetController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(actionSheetController, animated: true, completion: nil)
                
            }.addDisposableTo(disposeBag)
    }
    
    /**
        Description
        Hide or show the SearchBar. 
        Hide clear the search text and show the normal list
     **/
    
    private func toggleSearchBarVisibility(){
        
        searchBar.text = ""
        self.viewModel.searchCriteria.value = ""
        self.searchBarTopLayoutCostrain.constant = (self.searchBarTopLayoutCostrain.constant < 0) ? 0 : -44
        UIView.animate(withDuration: 0.3, animations: {[unowned self] in
            self.view.layoutIfNeeded()
        })
        
        if self.searchBarTopLayoutCostrain.constant == 0{
            searchBar.becomeFirstResponder()
        }else{
            searchBar.resignFirstResponder()
        }
        
        
        
    }
    
    @IBAction func onSearchClicked(_ sender: UIBarButtonItem) {
        toggleSearchBarVisibility()
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

