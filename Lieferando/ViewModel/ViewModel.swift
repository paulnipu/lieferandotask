//
//  ViewModel.swift
//  Lieferando
//
//  Created by Arup Paul on 19/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

class ViewModel{
    
    static let sharedInstance = ViewModel()
    //private var rawData = [String : Restaurent]()
    var restaurentList:Variable<[Restaurent]> = Variable([])
    
    var searchCriteria : Variable<String> = Variable("")
    var sortCriteria:SortOptions = .BEST_MATCH
    var sortType : SortStyle = .ASC
    var error:Variable<CustomError>?
    
    private let disposeBag = DisposeBag()
    
    private init(){
        
    }
    /**
        Description
        Parse Json the data from file and load the previous Favorite list
        The wjole restaurent list will then avalable via restaurentList Observable
     
        param : Error call back for notifying error to caller
    **/
    func parseData(errorCallback:((Error?) -> ())?){
        
        JsonParserUtil.sharedInstance.parseJson()
            .subscribeOn(ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global(qos: .default)))
            .observeOn(MainScheduler.instance)
            .map{[unowned self] responseModel in
                //self.loadFavorite(restaurents: responseModel.restaurents)
                self.saveData(restaurants: responseModel.restaurents)
            }
            .subscribe(onNext: {[unowned self] (isSaved) in
                //On Next
                //debugPrint(isSaved)
                self.sortData()
                }, onError: { (error) in
                    //On Error
                    errorCallback?(error)
                    debugPrint(error)
            }, onCompleted: {
                //OnCompleted
            }, onDisposed: {
                //On Deallocated
            }).addDisposableTo(disposeBag)
    }
    
    /**
        Description
        Sort Restaurent list according to Sort Criteria and sortStyle
     
        - Parameter by : EnumSortOptions.
        - Parameter style : SortStyle.
    
    **/
    func sortData(by:SortOptions = .BEST_MATCH,style:SortStyle = .ASC){
        sortCriteria = by
        sortType = style
        let sortByFavorite = NSSortDescriptor(key: "favourite", ascending: false)
        let sortByStatus = NSSortDescriptor(key: "sortstatus", ascending: true)
        let sortByCriteria = NSSortDescriptor(key: sortCriteria.rawValue, ascending : Bool(sortType.rawValue))
        
        restaurentList.value = ((restaurentList.value as NSArray).sortedArray(using: [sortByFavorite,sortByStatus,sortByCriteria])) as! Array<Restaurent>
    }
    
    
    /**
        Toggle favorite item
        
        - Parameter restaurant : Restaurant
    **/
    func toggleFavourite(restaurant:Restaurent){
        let realm = try! Realm()
        realm.beginWrite()
        restaurant.favourite = !restaurant.favourite
        try! realm.commitWrite()
        sortData(by: sortCriteria, style: sortType)
    }
    
    /**
        Search Restaurent by its name
        
        - Parameter searchParam : Restaurent name full or partially
    **/
    func searchRestaurent(searchParam:String){
        let realm = try! Realm()
        restaurentList.value = realm.objects(Restaurent.self).filter("name contains[c] %@", searchParam).map{$0}
        sortData()
    }
    
    
    /**
     
        Load favorite items from old databse and add new Data to the database.
        Delete Old data.
     
        - Parameter restaurents : List of Restaurent to save
    **/
    private func saveData(restaurants:[Restaurent]) -> Bool{
        
        let realm = try! Realm()
        //debugPrint(realm.configuration.fileURL ?? "")
        try! realm.write {
            realm.delete(realm.objects(Restaurent.self).filter("favourite = %@", false))
        }
        restaurants.forEach{restaurent in
            if let oldData = realm.object(ofType: Restaurent.self, forPrimaryKey: restaurent.name) {
                restaurent.favourite = oldData.favourite
            }
            try! realm.write {
                
                realm.add(restaurent, update: true)
                
            }
        }
        restaurentList.value = realm.objects(Restaurent.self).map{$0}
        searchCriteria
            .asObservable()
            .bindNext{ [unowned self] val in
                self.searchRestaurent(searchParam: val)
            }.addDisposableTo(disposeBag)
        return true
    }
    
    
    /***************** Done Without Realm ****************/
    
    /*func sortData(by:SortOptions = .BEST_MATCH,ascending:SortStyle = .ASC){
     sortCriteria = by
     sortType = ascending
     let sortByFavorite = NSSortDescriptor(key: "favourite", ascending: false)
     let sortByStatus = NSSortDescriptor(key: "sortstatus", ascending: true)
     let sortByCriteria = NSSortDescriptor(key: sortCriteria.rawValue, ascending : Bool(sortType.rawValue))
     
     restaurentList.value = ((Array(rawData.values) as NSArray).sortedArray(using: [sortByFavorite,sortByStatus,sortByCriteria])) as! Array<Restaurent>
     }*/
    
    /*func searchRestaurent(searchParam:String) {
     restaurentList.value = rawData.values.filter{$0.name?.contains(searchParam) ?? false}
     sortData(by:sortCriteria,ascending: sortType)
     }*/
    
    /*private func loadFavorite(restaurents:[Restaurent]) -> Bool{
     restaurents.forEach{ rawData[$0.name!] = $0}
     let realm = try! Realm()
     realm.objects(Restaurent.self).forEach{[unowned self] in
     self.rawData[$0.name ?? ""]?.favourite = true
     }
     return true
     }*/
    
    /*func toggleFavourite(restaurent:Restaurent){
     let realm = try! Realm()
     if restaurent.favourite{
     
     if let obj = realm.object(ofType: Restaurent.self, forPrimaryKey: restaurent.name ?? ""){
     try! realm.write {
     realm.delete(obj)
     }
     }
     
     }else{
     restaurent.favourite = true
     
     try! realm.write {
     
     realm.add(restaurent)
     }
     }
     }*/
    
    
}
