//
//  UIViewExtension.swift
//  Lieferando
//
//  Created by Arup Paul on 20/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import Foundation
import UIKit

extension UIView{

    @IBInspectable var roundedCornerRadius : CGFloat{
        get{
            return layer.cornerRadius
        }
        set(newVal) {
            layer.cornerRadius = newVal
        }
    }
    
}
