//
//  JsonParserUtil.swift
//  Lieferando
//
//  Created by Arup Paul on 19/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

class JsonParserUtil{
    
        
    static let sharedInstance = JsonParserUtil()
    
    private let jsonfile = Bundle.main.path(forResource: "sample", ofType: "json")
    
    private init(){
        
    }
    
    func parseJson(_ plainJson:String? = nil) -> Observable<ResponseModel>{
        return Observable<ResponseModel>.create { [unowned self] (subscriber) -> Disposable in
            do {
                let jsonData:String = try plainJson ?? String(contentsOfFile: self.jsonfile!)
                
                
                
                if let restaurents = Mapper<ResponseModel>().map(JSONString: jsonData){
                    subscriber.onNext(restaurents)
                    subscriber.onCompleted()
                }else{
                    subscriber.onError(CustomError.FAILED_TO_PARSE)
                    subscriber.onCompleted()
                }
                
            }
            catch {
                subscriber.onError(error)
                subscriber.onCompleted()
            }
            
            return Disposables.create()
        }
    }
    
    deinit {
        debugPrint("JsonParserUtil deallocated")
    }
}
