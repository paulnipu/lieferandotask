//
//  ExtensionTableView.swift
//  Lieferando
//
//  Created by Arup Paul on 22/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import Foundation
import UIKit

extension UITableView{
    
    func showNOitem(message:String){
        guard self.backgroundView == nil
            else{
                return
            }
        
        self.separatorStyle = .none
        let lblMessage = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        lblMessage.text = message
        lblMessage.textAlignment = .center
        lblMessage.textColor = UIColor.darkGray
        self.backgroundView = lblMessage
        
    }
    
    func hideNoItemMessage(){
        guard self.backgroundView != nil
            else{
                return
            }
        self.backgroundView?.removeFromSuperview()
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
