//
//  CustomCell.swift
//  Lieferando
//
//  Created by Arup Paul on 20/01/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import AvatarImageView

class CustomCell: UITableViewCell {
    
    typealias favClosure = (Restaurent?) -> ()
    
    private let disposeBag = DisposeBag.init()
    
    @IBOutlet private weak var lblRestaurentName: UILabel!
    
    @IBOutlet private weak var lblStatus: UILabel!
    
    @IBOutlet private weak var lblSortParam: UILabel!
    
    @IBOutlet private weak var lblSortVal: UILabel!
    
    @IBOutlet weak var btnFav: UIButton!
    
    @IBOutlet weak var imgRestaurent: AvatarImageView!{
        didSet{
            imgRestaurent.configuration = AvatarConfig()
        }
    }
    
    
    private var callback : favClosure?
    
    private var currentRestaurent:Restaurent?
    
    struct AvatarConfig: AvatarImageViewConfiguration {
        let shape: Shape = .circle
        let bgColor: UIColor? = UIColor.lightGray
    }
    
    struct AvatarDataSource : AvatarImageViewDataSource {
        var name : String
        var avatar : UIImage?
    }
    
    func setupData(restaurent:Restaurent,sortCriteria:SortOptions){
        currentRestaurent = restaurent
        lblRestaurentName.text = currentRestaurent?.name
        lblStatus.text = currentRestaurent?.status
        lblSortParam.text = "Sort by: " + sortCriteria.rawValue.capitalized
        lblSortVal.text = "Sort Value: \(restaurent.value(forKey: sortCriteria.rawValue) ?? 0)"
        imgRestaurent.dataSource = AvatarDataSource(name: restaurent.name ?? "", avatar: nil)
        
        var favImage = UIImage(named: "notfav")
        if currentRestaurent?.favourite ?? false{
            favImage = UIImage(named: "fav")
            
        }
        btnFav.setImage(favImage, for: .normal)
        btnFav.tintColor = UIColor.purple
        
        if currentRestaurent?.status?.lowercased() == "open"{
            lblStatus.textColor = UIColor(colorLiteralRed: 0, green: 0.6, blue: 0, alpha: 1)
        }else if currentRestaurent?.status?.lowercased() == "order ahead"{
            lblStatus.textColor = UIColor.orange
        }else{
            lblStatus.textColor = UIColor.red
        }
        
    }
    
    
    @IBAction private func onFavClicked(_ sender: UIButton) {
        callback?(currentRestaurent)
    }
    
    
    func favClicked(callback : @escaping favClosure) {
        self.callback = callback
    }
}
